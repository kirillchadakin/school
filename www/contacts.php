<?php
require_once "php_scripts/common.php";
// Вывод заголовка с данными о кодировке страницы
header('Content-Type: text/html; charset=utf-8');
// Настройка локали
setlocale(LC_ALL, 'ru_RU.65001', 'rus_RUS.65001', 'Russian_Russia. 65001', 'russian');
// Настройка подключения к базе данных
//mysql_query('SET names "utf8"');
?>

<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet">
    <!--    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">-->

    <title>Школа</title>

    <style>
        body {
            background-image: url(content/background.png);
            background-repeat: repeat;
        }
    </style>

    <style>
        .line-through {
            text-decoration: line-through;
        }
    </style>
</head>
<body>

<?php include_once "content/menu.php"?>

<div class="container mt-3 bg-white">
    <div class="row">
        <div class="col-4 align-items-center h-100">
            <img src="content/shcool1.JPG" class="w-100 m-3"/>
        </div>
        <div class="col-8">
            <h1>КОНТАКТЫ</h1>
            <h2>Контактные телефоны</h2>
            <ul class="list-group list-group-flush">
                <li class="list-group-item">76-47-49 – <b>директор</b></li>
                <li class="list-group-item">76-43-87 – канцелярия</li>
                <li class="list-group-item">76-47-78 – бухгалтерия, кабинет завучей</li>
                <li class="list-group-item">Факс: 76-43-87</li>
                <li class="list-group-item">Почтовый адрес: 390005, г. Рязань, ул. Дзержинского, д. 78</li>
                <li class="list-group-item">Электронная почта: school16ryazan@mail.ru</li>
            </ul>
        </div>
    </div>
</div>

<script src="outside_libs/jquery-3.6.0.min.js"></script>
<script src="bootstrap/js/bootstrap.bundle.js"></script>

</body>
</html>