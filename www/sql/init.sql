drop table homeworks;
drop table user;

create table user
(
    ID       int auto_increment primary key,
    login    varchar(50) not null,
    password varchar(50) not null,
    teacher  varchar(1)
);

create table homeworks
(
    ID            int auto_increment primary key,
    text          varchar(300) not null,
    teacher       int          not null,
    creation_date date,
    subject       varchar(50)
);

alter table homeworks
    add constraint FK_homeworks_user
        foreign key (teacher) references user (ID);

create table applications
(
    ID         int auto_increment primary key,
    name       varchar(100),
    lastName   varchar(100),
    fatherName varchar(100),
    age        int,
    sex        varchar(1),
    address    varchar(300),
    phone      varchar(15)
);

insert into applications (name, lastName, fatherName, age, sex, address, phone)
values ('Иван', 'Иванов', 'Иванович', 7, 'М', 'Рязань', '88005553535');
insert into applications (name, lastName, fatherName, age, sex, address, phone)
values ('Софья', 'Горбатова', 'Дмитриевна', 7, 'Ж', 'Рязань', '88005553535');

insert into user (ID, login, password, teacher)
values (1, 'krutulina_va', '123', 'T');
insert into user (ID, login, password, teacher)
values (2, 'klimuhina_av', '321', 'S');

insert into homeworks (ID, text, teacher, subject)
values (1, 'Прочитать параграф на 13 стр', 1, 'История');
insert into homeworks (ID, text, teacher, subject)
values (2, 'Упр стр 12', 1, 'Математика');

select *
from user;
select *
from homeworks;
select *
from applications;