<?php
session_start();
// Вывод заголовка с данными о кодировке страницы
header('Content-Type: text/html; charset=utf-8');
// Настройка локали
setlocale(LC_ALL, 'ru_RU.65001', 'rus_RUS.65001', 'Russian_Russia. 65001', 'russian');
// Настройка подключения к базе данных
//mysql_query('SET names "utf8"');
?>

<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <title>Самая лучшая школа!</title>
    <style>
        body {
            background-image: url(content/shcool.JPG);
            background-size: cover;
            background-repeat: no-repeat;
        }
        form {
            border-radius: 25%;
        }
    </style>
</head>
<body>

<form class="container w-25 bg-white" action="php_scripts/action/register.php" method="post">
    <h1 class="text-center mt-5 mb-5">Вход</h1>
    <input type="hidden" name="mode" value="login">
    <div class="form-group row justify-content-md-center">
        <label for="login ">Ваше имя</label>
        <input type="text" class="form-control m-3" id="login" name="login" placeholder="Введите логин">
        <label for="password">Пароль</label>
        <input type="password" class="form-control m-3" id="password" name="password" placeholder="Введите пароль">
        <a href="../about_us.php?guest=1" class="btn btn-link">Войти как гость</a>
    </div>
    <div class="form-group row justify-content-md-center">
        <button type="submit" class="btn btn-primary btn-lg m-3">Войти</button>
    </div>
    <?php
    if (!empty($_SESSION['login_error'])) {
        echo '
                <div class="form-group row justify-content-md-center">
			<label class="text-danger">'.$_SESSION['login_error'].'</label>
		</div>';
        unset($_SESSION['login_error']);
    }
    ?>

</form>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="outside_libs/jquery.js"></script>
<script src="outside_libs/popper.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
</body>
</html>