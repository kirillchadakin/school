<nav class="navbar navbar-expand-lg navbar-light bg-white">
    <div class="collapse navbar-collapse justify-content-center" id="navbarNavAltMarkup">
        <?php
        if ($_SESSION['guest']) {
            $fullLogin = 'Гость';
        } elseif ($_SESSION['role'] == 'S') {
            $fullLogin = 'Ученик ' . $_SESSION['login'];
        } elseif ($_SESSION['role'] == 'T') {
            $fullLogin = 'Учитель ' . $_SESSION['login'];
        }
        ?>
        <a class="navbar-brand" href="#"><?php echo $fullLogin; ?></a>
        <button class="navbar-toggler ml-7" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup"
                aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="navbar-nav">
            <?php
            $disableStudent = $_SESSION['guest'] != 1 && ($_SESSION['role'] == 'S' || $_SESSION['role'] == 'T') ? '' : 'disabled';
            $disableTeacher = $_SESSION['guest'] != 1 && $_SESSION['role'] == 'T' ? '' : 'disabled';
            $applicationsVisible = $_SESSION['guest'] != 1 && $_SESSION['role'] == 'T' ? '' : 'd-none';
            ?>
            <a class="nav-item nav-link active" href="about_us.php">О нас <span class="sr-only">(current)</span></a>
            <a class="nav-item nav-link" href="contacts.php">Контакты</a>
            <a class="nav-item nav-link" href="new_application.php">Прием заявок</a>
            <a class="nav-item nav-link <?php echo $disableStudent ?>" href="student_page.php">Список заданий</a>
            <a class="nav-item nav-link <?php echo $disableTeacher ?>" href="teacher_page.php">Ученики</a>
            <a class="nav-item nav-link <?php echo $applicationsVisible ?>" href="applications.php">Заявки</a>
            <a class="nav-item nav-link text-danger" href="php_scripts/action/logout.php">Выход</a>
        </div>
    </div>
</nav>
