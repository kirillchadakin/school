<?php
require_once "php_scripts/common.php";
require_once "php_scripts/database.php";
// Вывод заголовка с данными о кодировке страницы
header('Content-Type: text/html; charset=utf-8');
// Настройка локали
setlocale(LC_ALL, 'ru_RU.65001', 'rus_RUS.65001', 'Russian_Russia. 65001', 'russian');
// Настройка подключения к базе данных
//mysql_query('SET names "utf8"');
?>

<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet">
    <!--    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">-->

    <title>Школа</title>

    <style>
        body {
            background-image: url(content/background.png);
            background-repeat: repeat;
        }
        .big {
            font-size: 1.5em;
        }
    </style>

    <style>
        .line-through {
            text-decoration: line-through;
        }
    </style>
</head>
<body>

<?php include_once "content/menu.php"?>

<div class="container mt-4 bg-white">
    <div class="row justify-content-center">
        <h1>Ученики, посещавшие сайт</h1>
    </div>
    <div class="row justify-content-center">
        <ul class="list-group list-group-flush">
            <?php
            $fetch = db_get_all_students();
            foreach ($fetch as $row) {
                echo "<li class='list-group-item big'>$row[1]</li>";
            }
            ?>
        </ul>

<!--        <table class="table w-25">-->
<!--            <tr>-->
<!--                <th>id</th>-->
<!--                <th>login</th>-->
<!--            </tr>-->
<!--            -->
<!--        </table>-->
    </div>
</div>

<script src="outside_libs/jquery-3.6.0.min.js"></script>
<script src="bootstrap/js/bootstrap.bundle.js"></script>

</body>
</html>