<?php
require_once "php_scripts/common.php";
require_once "php_scripts/database.php";

if (isset($_POST['subject'])) {
    tlog($_POST['date']);
    db_add_homework($_POST['task'], $_SESSION['login'], $_POST['date'], $_POST['subject']);
    header('Location: /student_page.php');
}

header('Content-Type: text/html; charset=utf-8');
setlocale(LC_ALL, 'ru_RU.65001', 'rus_RUS.65001', 'Russian_Russia. 65001', 'russian');
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet">

    <title>Школа</title>

    <style>
        body {
            background-image: url(content/background.png);
            background-repeat: repeat;
        }
    </style>
</head>
<body>

<?php include_once "content/menu.php" ?>

<div class="container mt-3 bg-white rounded">
    <h1 class="text-center">Добавить домашнее задание</h1>
    <form method="post">
        <div class="mb-3">
            <label for="subject" class="form-label">Предмет</label>
            <input type="text" name="subject" class="form-control" id="subject" placeholder="Введите название предмета">
        </div>
        <div class="mb-3">
            <label for="task" class="form-label">Задание</label>
            <textarea class="form-control" name="task" id="task" rows="3"></textarea>
        </div>
        <div class="mb-3">
            <label for="date" class="form-label">Дата</label>
            <input type="date" class="form-control" name="date" id="date">
        </div>
        <div class="d-flex justify-content-center">
            <input type="submit" value="Сохранить" class="btn btn-lg btn-primary m-4" id="date">
        </div>
    </form>
</div>

<script src="outside_libs/jquery-3.6.0.min.js"></script>
<script src="bootstrap/js/bootstrap.bundle.js"></script>
</body>
</html>