<?php
require_once "php_scripts/common.php";
// Вывод заголовка с данными о кодировке страницы
header('Content-Type: text/html; charset=utf-8');
// Настройка локали
setlocale(LC_ALL, 'ru_RU.65001', 'rus_RUS.65001', 'Russian_Russia. 65001', 'russian');
// Настройка подключения к базе данных
//mysql_query('SET names "utf8"');
?>

<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet">
    <!--    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">-->

    <title>Школа</title>

    <style>
        body {
            background-image: url(content/background.png);
            background-repeat: repeat;
        }
    </style>

    <style>
        .line-through {
            text-decoration: line-through;
        }
    </style>
</head>
<body>

<?php include_once "content/menu.php"?>

<div class="container mt-3 ">
    <div class="row justify-content-center bg-white w-50 m-auto">
        <h1>Заполеннеие заявки</h1>
    </div>
    <div class="row justify-content-center bg-white w-50 m-auto">
        <form class="p-5 rounded">
            <div class="form-group">
                <label for="nameInput">Имя</label>
                <input type="text" class="form-control" id="nameInput" placeholder="Введите имя">
            </div>
            <div class="form-group">
                <label for="nameInput">Фамилия</label>
                <input type="text" class="form-control" id="nameInput" placeholder="Введите фамилию">
            </div>
            <div class="form-group">
                <label for="nameInput">Отчество</label>
                <input type="text" class="form-control" id="nameInput" placeholder="Введите отчество">
            </div>
            <div class="form-group">
                <label for="nameInput">Возраст</label>
                <input type="number" class="form-control" id="nameInput">
            </div>
            <div class="form-group">
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked>
                    <label class="form-check-label" for="exampleRadios1">
                        Мужской
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="option2">
                    <label class="form-check-label" for="exampleRadios2">
                        Женский
                    </label>
                </div>
            </div>
            <div class="form-group">
                <label for="nameInput">Адрес прописки</label>
                <input type="text" class="form-control" id="nameInput" placeholder="Введите адрес прописки">
            </div>
            <div class="form-group">
                <label for="nameInput">Телефон родителя</label>
                <input type="tel" class="form-control" id="nameInput">
            </div>
            <button type="submit" class="btn btn-primary">Оставить заявку</button>
        </form>
    </div>
</div>

<script src="outside_libs/jquery-3.6.0.min.js"></script>
<script src="bootstrap/js/bootstrap.bundle.js"></script>

</body>
</html>