<?php
require_once "php_scripts/common.php";
// Вывод заголовка с данными о кодировке страницы
header('Content-Type: text/html; charset=utf-8');
// Настройка локали
setlocale(LC_ALL, 'ru_RU.65001', 'rus_RUS.65001', 'Russian_Russia. 65001', 'russian');
// Настройка подключения к базе данных
//mysql_query('SET names "utf8"');
?>

<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet">
    <!--    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">-->

    <title>Школа</title>

    <style>
        body {
            background-image: url(content/background.png);
            background-repeat: repeat;
        }
    </style>

    <style>
        .line-through {
            text-decoration: line-through;
        }
    </style>
</head>
<body>

<?php include_once "content/menu.php"?>

<div class="container mt-3 bg-white">
    <div class="row justify-content-center text-uppercase">
        <h1>О ШКОЛЕ</h1>
    </div>
    <div class="row">
        <div class="col">
            <img src="content/shcool2.JPG"  class="float-right m-3 w-50"/>
            <h2>ОСНОВНЫЕ СВЕДЕНИЯ</h2>
            <p>Тип, вид, статус учреждения: Муниципальное бюджетное общеобразовательное  учреждение «Школа № 16» (МБОУ «Школа № 16»).</p>
            <p>Дата основания:  1976 год.</p>
            <p>Адрес: 390005, г.Рязань, улица Дзержинского, 78.</p>
            <p>Телефон: 4(912) 76-43-87 -канцелярия</p>
            <p>4(912) 76-47-49 — директор.</p>
            <p>4(4912) 76-47-78 — завучи.</p>
            <p>Адрес электронной почты: school16ryazan@mail.ru</p>
            <p>Филиалы: нет.</p>
            <p>Учредитель образовательного учреждения: муниципальное образование — городской округ город Рязань Рязанской области в лице Рязанской городской Думы и администрации города Рязани.</p>
            <p>Лицензия на образовательную деятельность № 27-2211 от 08.06.2015 г., выдана министерством образования Рязанской области; свидетельство о государственной аккредитации 27-0734 от 08.06.2015 г., выдано министерством образования Рязанской области.</p>
            <p>Характеристика контингента учащихся:на начало 2017-2018  учебного года количество обучающихся составляло 961 человек, 36 класса-комплекта: начальная школа – 14 классов, основная школа – 18, старшая – 4.</p>

            <h2>Сведения о различных категориях обучающихся</h2>
            <p>Трудоустройство учащихся 2016-2017 уч.год</p>
            <p>Средняя наполняемость классов – 27 человек. Согласно распорядку дня учебные занятия в школе проводятся в одну смену.</p>
            <p>Директор школы: Бабаева Ольга Викторовна — Почетный работник общего образования РФ.</p>
            <p>Деятельность школы по вопросам организации образовательного процесса, содержания образования и другим вопросам функционирования и развития регламентируется федеральными, региональными  нормативными документами, а также внутренними локальными актами: правилами внутреннего распорядка, правами и правилами для учащихся, внутренними положениями, приказами и инструкциями.</p>
            <p>Орган государственно-общественного управления: Совет МБОУ «Школа №16».</p>

            <h3>Основные общеобразовательные программы:</h3>
            <ul>
                <li>основная общеобразовательная программа начального общего образования;</li>
                <li>основная общеобразовательная программа основного общего образования;</li>
                <li>основная общеобразовательная программа среднего (полного) общего образования;</li>
                <li>дополнительное образование детей и взрослых.</li>
            </ul>

            <h3>Статус классов:</h3>
            <ul>
                <li>общеобразовательные</li>
                <li>профильные</li>
            </ul>

            <h3>Направления профилей:</h3>
            <ul>
                <li>физико-химический;</li>
                <li>физико-математический.</li>
            </ul>

            <h3>На базе школы функционируют:</h3>
            <ul>
                <li>МБОУ ДОД «Детская муниципальная школа № 5 имени В.Ф. Бобылева»,</li>
                <li>МБОУ ДОД «Детская музыкальная хоровая школа № 8»,</li>
                <li>МБОУ ДОД специализированная детско-юношеская спортивная школа олимпийского резерва «Антей»</li>
                <li> МБОУ ДОД детско-юношеская спортивная школа «Фаворит»</li>
                <li>МБОУ ДОД «детско-юношеский центр «Спорттур»»</li>
            </ul>

            <p>В школе организовано изучение английского языка со второго класса.</p>

            <h3>На базе школе действуют следующие кружки и секции:</h3>
            <ul>
                <li>баскетбол,</li>
                <li>футбол,</li>
                <li>спортивное ориентирование,</li>
                <li>бильярд,</li>
                <li>теннис,</li>
                <li>хореография,</li>
                <li>научное общество учащихся на базе школьного музея имени Э.К. Циолковского,</li>
                <li>изостудия,</li>
                <li>основы выразительного чтения,</li>
                <li>вокал.</li>
            </ul>
            <p>Психолого-социальное сопровождение в школе осуществляется  социальным педагогом и психологом.</p>
            <p>Медицинские услуги оказывает квалифицированный врач и медицинская сестра из детской поликлиники №1.</p>
        </div>
    </div>
</div>

<script src="outside_libs/jquery-3.6.0.min.js"></script>
<script src="bootstrap/js/bootstrap.bundle.js"></script>

</body>
</html>