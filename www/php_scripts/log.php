<?php
function tlog($string){
    $log_file_name = $_SERVER['DOCUMENT_ROOT']."/logs.txt";
    $now = date("Y-m-d H:i:s");
    file_put_contents($log_file_name, $now." ".$string."\r\n", FILE_APPEND);
}