<?php
include_once "log.php";
session_start();
tlog('Come into '.$_SERVER['REQUEST_URI'] . ', guest='. $_SESSION['guest']);
if ($_POST['action'] == 'register') {
    require_once "action/register.php";
} elseif ($_GET['guest'] == 1) {
    $_SESSION['guest'] = true;
} elseif (!isset($_SESSION['login']) && !$_SESSION['guest']) {
    header('Location: /');
}
