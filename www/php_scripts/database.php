<?php
require_once "User.php";
include_once "log.php";

function createConnection()
{
    $host = 'localhost'; // адрес сервера
    $database = 'school'; // имя базы данных
    $user = 'root'; // имя пользователя
    $password = ''; // пароль
    $link = mysqli_connect($host, $user, $password, $database)
    or die("Ошибка " . mysqli_error($link));
    //mysqli_set_charset($link, "utf8");
    return $link;
}

function db_add_homework($text, $teacherLogin, $date, $subject)
{
    request("insert into homeworks (text, teacher, creation_date, subject)
values ('$text', (select id from user where login='$teacherLogin'), '$date', '$subject')");
}

function db_is_password_correct($login, $password)
{
    $result = request("SELECT * FROM `user` WHERE login='$login' AND password='$password'");
    if (mysqli_num_rows($result) == 0) {
        tlog('User ' . $login . ' not found');
        return false;
    }
    $row = mysqli_fetch_row($result);
    tlog('user ' . $row[1] . ' role: ' . $row[3]);
    return $row[3];
}

function db_get_applications()
{
    $result = request("select name, lastname, fathername, age, sex, address, phone from applications");
    return mysqli_fetch_all($result);
}

function db_get_all_students()
{
    $result = request("SELECT * FROM user WHERE teacher='S'");
    return mysqli_fetch_all($result);
}

function db_get_homework()
{
    $result = request("SELECT subject, text, creation_date, login  FROM `user` u inner join homeworks h on u.id = h.teacher");
    return mysqli_fetch_all($result);
}

function db_new_user($login, $password)
{
    $connection = createConnection();
    $result = mysqli_query($connection, "INSERT INTO `users` (`name`, `password`) VALUES ('$login', '$password')");
    if ($result) {
        $result = mysqli_query($connection, "SELECT * FROM `users` WHERE name='$login'");
        $row = mysqli_fetch_row($result);
        $id = $row[0];
        tlog("User with id=$id inserted");
        mysqli_close($connection);
        return new User($row[0], $row[1]);
    }
    tlog("Inserting fail");
    mysqli_close($connection);
    return false;
}

function db_get_user($login)
{
    $connection = createConnection();
    $result = mysqli_query($connection, "SELECT * FROM `users` WHERE name='$login'");
    if (mysqli_num_rows($result) != 0) {
        $row = mysqli_fetch_row($result);
        tlog("User got from db:" . $row[0] . ' ' . $row[1]);
        mysqli_close($connection);
        return new User($row[0], $row[1]);
    }
    $result = mysqli_query($connection, "INSERT INTO `users` (`name`) VALUES ('$login')");
    tlog("User not found, executing insert...");
    if ($result) {
        $result = mysqli_query($connection, "SELECT * FROM `users` WHERE name='$login'");
        $row = mysqli_fetch_row($result);
        $id = $row[0];
        tlog("User with id=$id inserted");
        mysqli_close($connection);
        return new User($row[0], $row[1]);
    }
    tlog("Inserting fail");
    mysqli_close($connection);
    return false;
}

function db_update_node_status($nodeId, $nodeStatus)
{
    request("UPDATE `nodes` SET `done`=$nodeStatus WHERE id=$nodeId");
}

function db_delete_node($nodeId)
{
    request("delete from nodes where id = $nodeId");
}

function db_update_node($nodeId, $newContent)
{
    request("update nodes set content = '$newContent' where id = $nodeId");
}

function db_insert_new_node($userId, $content)
{
    request("insert into nodes (content, author) VALUE ('$content', $userId)");
}


function db_get_user_nodes($userId)
{
    $result = request("select * from nodes where author=$userId");
    return createNodes($result);
}

function db_get_user_not_done_nodes($userId)
{
    $result = request("select * from nodes where author=$userId and done=0");
    return createNodes($result);
}

function db_get_user_done_nodes($userId)
{
    $result = request("select * from nodes where author=$userId and done=1");
    return createNodes($result);
}

function db_get_all_nodes()
{
    $result = request("SELECT id, content FROM `nodes`");
    return createNodes($result);
}

function db_get_node($id)
{
    $result = request("SELECT id, content FROM `nodes` WHERE id=$id");
    $fetch = mysqli_fetch_all($result, MYSQLI_ASSOC); //change to fetch_row
    return new Node($fetch[0]['id'], $fetch[0]['content']);
}


function request($query)
{
    $link = createConnection() or die("Error " . mysqli_error($link));
    $result = mysqli_query($link, $query) or die("Error in $query\n " . mysqli_error($link));
    mysqli_close($link);
    return $result;
}

function createNodes($result)
{
    $fetch = mysqli_fetch_all($result, MYSQLI_ASSOC);
    $nodes = array();
    foreach ($fetch as $row) {
        array_push($nodes, new Node($row['id'], $row['content'], $row['done']));
    }
    return $nodes;
}
