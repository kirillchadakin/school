<?php
require_once "../php_scripts/database.php";
require_once "../php_scripts/log.php";
if (isset($_POST['nodeId']) && isset($_POST['isDone'])) {
    db_update_node_status($_POST['nodeId'], $_POST['isDone']);
    $message = 'Updated ' . $_POST['nodeId'] . $_POST['isDone'];
    tlog($message);
    echo $message;
} else {
    tlog('Error updating');
    echo 'Error updating data';
}

