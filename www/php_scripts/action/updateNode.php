<?php
require_once "../php_scripts/database.php";
if ($_GET['node_id'] == 'new') {
    session_start();
    db_insert_new_node($_SESSION['user_id'], $_GET['content']);
} else {
    db_update_node($_GET['node_id'], $_GET['content']);
}
header("Location: /");