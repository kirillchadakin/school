<?php
require_once "../database.php";
session_start();
if (isset($_POST["login"]) && isset($_POST['password'])) {
    $login = $_POST["login"];
    $password = $_POST['password'];
    $role = db_is_password_correct($login, $password);
    if ($role) {
        $_SESSION['guest'] = 0;
        $_SESSION['login'] = $login;
        $_SESSION['role'] = $role;
        if ($role == 'T') {
            header("Location: /teacher_page.php");
        } else if ($role == 'S') {
            header("Location: /student_page.php");
        }
    } else {
        header("Location: /");
    }
}


