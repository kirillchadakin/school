<?php
require_once "php_scripts/common.php";
require_once "php_scripts/database.php";
// Вывод заголовка с данными о кодировке страницы
header('Content-Type: text/html; charset=utf-8');
// Настройка локали
setlocale(LC_ALL, 'ru_RU.65001', 'rus_RUS.65001', 'Russian_Russia. 65001', 'russian');
// Настройка подключения к базе данных
//mysql_query('SET names "utf8"');
?>

<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet">
    <!--    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">-->

    <title>Школа</title>

    <style>
        body {
            background-image: url(content/background.png);
            background-repeat: repeat;
        }
    </style>

    <style>
        .line-through {
            text-decoration: line-through;
        }
    </style>
</head>
<body>

<?php include_once "content/menu.php" ?>

<div class="container mt-3 bg-white rounded">
    <h1 class="text-center">Домашнее задание</h1>
    <table class="table">
        <tr>
            <th>Предмет</th>
            <th>Задание</th>
            <th>Дата</th>
            <th>Преподаватель</th>
        </tr>
        <?php
        $fetch = db_get_homework();
        foreach ($fetch as $row) {
            echo "<tr> <td>" . $row[0] . "</td> <td>" . $row[1] . "</td> <td>" . $row[2] . "</td> <td>" . $row[3] . "</td> </tr>";
        }
        ?>
    </table>
    <?php if ($_SESSION['role'] == 'T') {
        echo '<div class="row justify-content-center">
    <a href="new_homework.php" class="btn btn-primary btn-lg m-4">Добавить домашнюю работу</a>
</div>';
    }?>
</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="outside_libs/jquery-3.6.0.min.js"></script>
<!--<script src="outside_libs/popper.js"></script>-->
<script src="bootstrap/js/bootstrap.bundle.js"></script>


<!--<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"-->
<!--        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"-->
<!--        crossorigin="anonymous"></script>-->
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"-->
<!--        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"-->
<!--        crossorigin="anonymous"></script>-->
<!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"-->
<!--        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"-->
<!--        crossorigin="anonymous"></script>-->
</body>
</html>