<?php
require_once "php_scripts/common.php";
require_once "php_scripts/database.php";
// Вывод заголовка с данными о кодировке страницы
header('Content-Type: text/html; charset=utf-8');
// Настройка локали
setlocale(LC_ALL, 'ru_RU.65001', 'rus_RUS.65001', 'Russian_Russia. 65001', 'russian');
// Настройка подключения к базе данных
//mysql_query('SET names "utf8"');
?>

<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet">
    <!--    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">-->

    <title>Школа</title>

    <style>
        body {
            background-image: url(content/background.png);
            background-repeat: repeat;
        }
    </style>

    <style>
        .line-through {
            text-decoration: line-through;
        }
    </style>
</head>
<body>

<?php include_once "content/menu.php" ?>

<div class="container mt-3 bg-white rounded">
    <h1 class="text-center">Заявки</h1>
    <table class="table">
        <tr>
            <th>Имя</th>
            <th>Фамилия</th>
            <th>Отчество</th>
            <th>Возраст</th>
            <th>Пол</th>
            <th>Прописка</th>
            <th>Телефон</th>
        </tr>
        <?php
        $fetch = db_get_applications();
        foreach ($fetch as $row) {
            echo "<tr> 
                <td>" . $row[0] . "</td> 
                <td>" . $row[1] . "</td> 
                <td>" . $row[2] . "</td>
                <td>" . $row[3] . "</td> 
                <td>" . $row[4] . "</td> 
                <td>" . $row[5] . "</td>  
                <td>" . $row[6] . "</td> </tr>";
        }
        ?>
    </table>
</div>';
    }?>
</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="outside_libs/jquery-3.6.0.min.js"></script>
<!--<script src="outside_libs/popper.js"></script>-->
<script src="bootstrap/js/bootstrap.bundle.js"></script>

</body>
</html>